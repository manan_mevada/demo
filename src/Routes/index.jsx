import React from 'react';
import { Fragment } from 'react';
import { Routes, Route } from 'react-router-dom'; 
import Login from '../Component/Login'
import SignUp from '../Component/SignUp'
import Dashboard from '../Component/Dashboard'
import Audit from '../Component/Audit'

function RoutesList() {
  return (
    <Fragment>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/audit" element={<Audit />} />
      </Routes>
    </Fragment>
  )
}

export default RoutesList