import React from 'react'
import Header from '../../Common/Header'
import Table from 'react-bootstrap/Table';
var moment = require('moment');

function Dashboard() {
  const userData = JSON.parse(localStorage.getItem('userData'));
  const list = JSON.parse(localStorage.getItem('data'));
  return (
    <>
      <Header isNormalUser={userData.Role === 'USER' ? true : false} />
      <h3 style={{ width: '100%', textAlign: 'center' }}>Dashboard Page</h3>
      <Table responsive className='mt-5'>
        <thead>
          <tr>
            <th>#</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Last Login Date</th>
            <th>Ip</th>
          </tr>
        </thead>
        <tbody>
          {
            list.map((data, index) => {
              return (
                <tr>
                  <td>{index+1}</td>
                  <td>{data.firstName}</td>
                  <td>{data.lastName}</td>
                  <td>{data.email}</td>
                  <td>{data.Role}</td>
                  <td>{moment(data.loginTime).format('DD/MM/YYYY hh:mm:ss')}</td>
                  <td>{data.ipAddress}</td>
                </tr>
              )
            })
          }
        </tbody>
      </Table>
    </>
  )
}

export default Dashboard