import React, { useState } from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import { useNavigate } from 'react-router-dom';

function SignUp() {
  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [Role, setRole] = useState('')
  const navigate = useNavigate();
  const list = JSON.parse(localStorage.getItem('data'));

  const signUp = () => {
    const allData = list || []
    const newData = [...allData, {
      firstName,
      lastName,
      email,
      password,
      Role
    }]
    localStorage.setItem('data', JSON.stringify(newData))
    navigate('/')
  }
  return (
    <>
      <header className="App-header">
        <Card>
          <Card.Header>Signup Page</Card.Header>
          <Card.Body>
            <Form>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>First name</Form.Label>
                <Form.Control type="text" placeholder="name@example.com" value={firstName} onChange={(e) => {
                  setFirstName(e.target.value)
                }} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Last name</Form.Label>
                <Form.Control type="text" placeholder="name@example.com" value={lastName} onChange={(e) => {
                  setLastName(e.target.value)
                }} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Email Id</Form.Label>
                <Form.Control type="email" placeholder="name@example.com" value={email} onChange={(e) => {
                  setEmail(e.target.value)
                }} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => {
                  setPassword(e.target.value)
                }} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Role</Form.Label>
                <Form.Select aria-label="Default select example" value={Role} onChange={(e) => {
                  setRole(e.target.value)
                }} >
                  <option value="">Select Role</option>
                  <option value="USER">USER</option>
                  <option value="AUDITOR">AUDITOR</option>
                </Form.Select>
              </Form.Group>
            </Form>
            <Button variant="primary" onClick={signUp}>Submit</Button>
          </Card.Body>
            <Button variant="link" style={{ "text-align": 'right' }} onClick={() => {
              navigate('/')
            }}>login</Button>
        </Card>
      </header>
    </>
  )
}

export default SignUp