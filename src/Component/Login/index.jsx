import React, { useState, useEffect } from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import { useNavigate } from 'react-router-dom';

function Login() {
  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')
  const navigate = useNavigate();
  const list = JSON.parse(localStorage.getItem('data'));
  const userData = JSON.parse(localStorage.getItem('userData'));

  useEffect(() => {
    if (userData && userData.email) {
      if (userData.Role === 'USER') {
        navigate('/dashboard')
      } else {
        navigate('/audit')
      }
    }
  }, [])
  
  const loginClick = () => {
    if (password !== '' && email !== '') {
      const getData = list.filter(dList => (dList.email === email && dList.password === password))
      if (getData?.length) {
        const updatedData = list.map(dList => (dList.email === email && dList.password === password) ? {
          ...dList,
          loginTime: new Date(),
          ipAddress: window.location.origin
        } : dList);

        localStorage.setItem('data', JSON.stringify(updatedData))

        localStorage.setItem('userData', JSON.stringify(getData[0]))
        console.log('getData', getData)
        if (getData[0].Role === 'USER') {
          navigate('/dashboard')
        } else {
          navigate('/audit')
        }
      } else {
        alert('Please enter proper credential')
      }
    } else {
      alert('please enter all details')
    }
  }

  return (
    <>
      <header className="App-header">
        <Card>
          <Card.Header>Login Page</Card.Header>
          <Card.Body>
            <Form>
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Email Id</Form.Label>
                <Form.Control type="email" placeholder="name@example.com" value={email} onChange={(e) => {
                  setEmail(e.target.value)
                }} />
              </Form.Group>
              <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => {
                  setPassword(e.target.value)
                }} />
              </Form.Group>
            </Form>
            <Button variant="primary" onClick={loginClick}>Submit</Button>
          </Card.Body>
            <Button variant="link" style={{ "text-align": 'right' }} onClick={() => {
              navigate('/signup')
            }}>sign up</Button>
        </Card>
      </header>
    </>
  )
}

export default Login