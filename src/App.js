import './App.css';
import Routes from './Routes/index.jsx'

function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
