import React from 'react'
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { useNavigate } from 'react-router-dom';

function Header({ isNormalUser }) {
  const navigate = useNavigate();
  return (
    <Navbar>
      <Container>
        <Navbar.Brand>Byteridge</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            {
              isNormalUser === false && (
                <Nav.Link href="/audit" active={window.location.pathname === '/audit'}>Audit</Nav.Link>
              )
            }
            <Nav.Link href="/dashboard" active={window.location.pathname === '/dashboard'}>Dashboard</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            <Button onClick={() => {
              localStorage.removeItem('userData')
              navigate('/')
            }}>Logout</Button>
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default Header